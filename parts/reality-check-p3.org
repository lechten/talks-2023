    - What to do with *revised* contents?
      {{{reveallicense("./figures/thenounproject/noun_Conflict_1744722.meta","10rh")}}}
      - Do you *notify* the author?  Do you *collaborate*?  How?
      - How and where do you *redistribute*?
